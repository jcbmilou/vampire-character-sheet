import React, { Component } from 'react';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware, compose } from 'redux';
import thunk from 'redux-thunk';
import { reduxFirestore, getFirestore} from 'redux-firestore';
import { reactReduxFirebase, getFirebase, reduxReactFirebase} from 'react-redux-firebase';

import './css/normalize.css';
import './css/App.scss';
import firebaseConf from './config/firebase'
import reducers from './reducers';
import CharSheetApp from './components/CharSheetApp';

const composeEnhancers =
  typeof window === 'object' &&
  window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ ?   
    window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__({
      // Specify extension’s options like name, actionsBlacklist, actionsCreators, serialize...
    }) : compose;

const enhancer = composeEnhancers(
  applyMiddleware(thunk.withExtraArgument({getFirebase, getFirestore})),
  reduxFirestore(firebaseConf),
  reduxReactFirebase(firebaseConf)
);

class App extends Component {
  render() {
    return (
      <Provider store={createStore(reducers, 
        enhancer
      )}>
        <div className="App">
          <CharSheetApp />
        </div>
      </Provider>
    );
  }
}

export default App;
