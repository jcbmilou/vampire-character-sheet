const traits = (state = {}, action) => {
    switch(action.type) {
        case 'UPDATE_TRAITS':
            return {
                ...state,
                traits:{...state.traits, ...action.traits.traits}
            }
        default:
            return state
    }
}

export default traits;