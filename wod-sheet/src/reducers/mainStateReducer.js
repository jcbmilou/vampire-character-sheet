import data from '../data/defaultMainState.json';

const traits = (state = data, action) => {
    switch(action.type) {
        case 'UPDATE_MAIN_STATE':
            return {
                ...state,
                mainState:{...state.mainState, ...action.mainState.mainState}
            }
        case 'CREATE_CHARACTER':
            console.log('char saved');
            return state;
        
        case 'UPADTE_CHARACTER':
            console.log('char saved');
            return state;

        case 'ERROR':
            console.log('something went wrong', action.err);
            return state;
        default:
            return state
    }
}

export default traits;