import { combineReducers } from 'redux';
import { firestoreReducer } from 'redux-firestore';

import traitsReducer from './traitsReducer';
import descriptionReducer from './descriptionReducer';
import mainStateReducer from './mainStateReducer';

export default combineReducers({
    traits: traitsReducer,
    description: descriptionReducer,
    mainState: mainStateReducer,
    firestore: firestoreReducer
});