const description = (state = {}, action) => {
    switch(action.type) {
        case "FETCH_DESCRIPTION":
            return {
                ...state,
                description: action.description
            }
        case "UPDATE_DESCRIPTION":
            return {
                ...state,
                description: {...state.description, ...action.description.description}
            }
        default:
            return state
    }
}

export default description;