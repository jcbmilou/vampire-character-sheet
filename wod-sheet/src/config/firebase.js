import firebase from 'firebase/app';
import 'firebase/firestore' ;
import 'firebase/auth';

var config = {
    apiKey: "AIzaSyDDjT3siS0fuVq2919YW0ZDDom85rxIU7g",
    authDomain: "vamipire-character-sheet.firebaseapp.com",
    databaseURL: "https://vamipire-character-sheet.firebaseio.com",
    projectId: "vamipire-character-sheet",
    storageBucket: "vamipire-character-sheet.appspot.com",
    messagingSenderId: "631227745003"
};
firebase.initializeApp(config);
firebase.firestore().settings({timestampsInSnapshots: true});

export default firebase;
