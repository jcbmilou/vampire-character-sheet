import React, { Component } from 'react';
import { connect } from 'react-redux';

import * as actions from '../../actions'
import Traits from './traits';
import IndividualTrait from './individualtrait';


class Editables extends Component {
    state = {
        inputValue: "",
        newNote:""
    }

    changeCheckboxAmount = (action) => {
        const { traits, updateTraits, name } = this.props;
        const value = this.props.traits.traits["04editables"][name]
        if(value[0] < 50 && action === 'add') {
            traits.traits["04editables"][name][0] = ++value[0]
        } else if(value[0] > 1 && action === 'remove') {
            traits.traits["04editables"][name][0] = --value[0]
        } else {
            return;
        }

        updateTraits(traits);
    }

    checkBoxFill = e => {
        const { mainState, traits, name, updateTraits } = this.props;

        if(name === "02willpower") {
            if(e.currentTarget.classList.contains('checked')) {
                traits.traits["04editables"][name].willpower[1]--
            } else {
                traits.traits["04editables"][name].willpower[1]++
                
            }
        } else{
            if(e.currentTarget.classList.contains('checked')) {
                traits.traits["04editables"][name][1]--
            } else {
                traits.traits["04editables"][name][1]++
                
            }
        }

        updateTraits(traits);

        if(!mainState.mainState.characterCreation) {
            this.updateCharacater();
        }
    }

    updateCharacater = () => {
        const { traits, description, firestore, updateCharacter } = this.props;

        updateCharacter(traits, description, firestore);
    }

    damageCounter = (operator, dmgType) => {
        const {traits, updateTraits} = this.props;
        const items = traits.traits["04editables"].health;
        let flag = true;

        if (operator === "add") {
            Object.keys(items).map((item) =>  { 
                if (flag && items[item].value < 2) {
                        if(dmgType === "aggravated") {
                            if (items[item].value + 2 > 2) {
                                items[item].value++;
                                dmgType = "bash";
                            } else {
                                items[item].value= items[item].value + 2;
                                flag = false;
                            }
                        } else {
                            items[item].value++;
                            flag = false;
                        }
                    }
                return flag;
                });
        } else {
            Object.keys(items).reverse().map((item) =>  { 
                if (flag && items[item].value > 0) {
                        if(dmgType === "aggravated") {
                            if (items[item].value - 2 < 0) {
                                items[item].value--;
                                dmgType = "bash";
                            } else {
                                items[item].value= items[item].value - 2;
                                flag = false;
                            }
                        } else {
                            items[item].value--;
                            flag = false;
                        }
                    }
                return flag;
            });
        }

        updateTraits(traits);
        this.updateCharacater();
    }

    setInputValue = (e) => {
        this.setState({inputValue: e.target.value});
    }

    updateNote = (e) => {
        this.setState({newNote: e.target.value});
    }

    saveNote = () => {
        const {traits, updateTraits, name} = this.props;

        traits.traits["04editables"][name].push(this.state.newNote);
        updateTraits(traits);
        this.updateCharacater();
    }

    renderEditables = () => {
        const { traits, name, mainState} = this.props;

        const checkboxes = () => {
            let current = traits.traits["04editables"][name];
             if(name === "02willpower") {
                current = traits.traits["04editables"][name].willpower;
             }

            return [...Array(current[0])].map(
                (e, i) => {
                    if(i < current[1]) {
                        return (
                            <label className="check-container" key={i}>
                                <span className="checkmark checked" onClick={e => {this.checkBoxFill(e)}}></span>
                            </label>
                        )
                    } else {
                        return (
                            <label className="check-container" key={i}>
                                <span className="checkmark" onClick={e => {this.checkBoxFill(e)}}></span>
                            </label>
                        )
                    }
                }
            );
        }

        switch(name) {
            case "03blood_pool":
                return (
                    <div className="bloodpool">
                        <p>Blood Pool</p>
                        <div className="checkboxes">{checkboxes()}</div>
                        {mainState.mainState.characterCreation && 
                            <div className="edit-buttons">
                                <button onClick={() => {this.changeCheckboxAmount('add')}}>+</button>
                                <button onClick={() => {this.changeCheckboxAmount('remove')}}>-</button>
                            </div>
                        }
                    </div>
                )
            case "health":
                const healthFields = Object.keys(traits.traits["04editables"][name]).map( healthStatus => {
                    const currentHealthObj = traits.traits["04editables"].health[healthStatus];

                    return (
                        <tr key={healthStatus}>
                            <td>{healthStatus}</td>
                            <td>{currentHealthObj.reduction}</td>
                            <td className="checkboxField"> 
                                <span className={`checkmark-${currentHealthObj.value}`}></span>
                            </td>
                        </tr>
                    )
                });
                return (
                    !mainState.mainState.characterCreation &&
                    <div className="health">
                        Health
                        <table>
                            <tbody>
                                {healthFields}
                            </tbody>
                        </table>
                        <div>
                            HIT
                            <button onClick={() => {this.damageCounter('add', 'bash')}}>Bash</button>
                            <button onClick={() => {this.damageCounter('add', 'lethal')}}>Lethal</button>
                            <button onClick={() => {this.damageCounter('add', 'aggravated')}}>Aggravated</button>
                        </div>
                        <div>
                            HEAL
                            <button onClick={() => {this.damageCounter('remove', 'bash')}}>Bash</button>
                            <button onClick={() => {this.damageCounter('remove', 'lethal')}}>Lethal</button>
                            <button onClick={() => {this.damageCounter('remove', 'aggravated')}}>Aggravated</button>
                        </div>
                    </div>
                )
            case "notes":
                const renderNotes = traits.traits["04editables"][name].map(note => {
                    return <p key="name">{note}</p>
                })
                return (
                    <div className="notes">
                        Notes
                        {renderNotes}
                       <textarea onChange={(e) => this.updateNote(e)}>
                       </textarea>
                       <button onClick={this.saveNote}>
                           Save Note
                       </button>
                    </div>
                )
            case "01path":
                return (
                    <Traits title="04editables" subtitle="01path" traits={traits} />
                )
            case "02willpower":
                return (
                    <div className="willpower">
                        <Traits title="04editables" subtitle="02willpower" traits={traits} />
                        <div className="checkboxes">{checkboxes()}</div>
                    </div>
                )
            case "experience":
                return
            default:
                return 
        }
    }

    render() {
        const {traits, name, mainState} = this.props

        return(
            <React.Fragment>
                {this.renderEditables()}
            </React.Fragment>
        )
    }
}

const mapStateToProps = ({ mainState, firestore, description }) => {
    return {
        mainState: mainState,
        firestore: firestore.ordered.Characters,
        description: description
    }
}

export default connect(mapStateToProps, actions)(Editables);