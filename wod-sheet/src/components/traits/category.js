import React, { Component }  from 'react';
import { connect } from 'react-redux';
import * as actions from '../../actions';

import SubCategory from './subcategory';

class Category extends Component {
    closeDetails = () => {
        this.props.mainState.mainState.toggleDetails = "close";
    
        this.props.updateMainState(this.props.mainState)
    }

    render() {
        const { traits, mainState } = this.props;
        const renderTraitTitle = Object.keys(traits.traits).map( title => {
            if(title !== "04editables") {
                return <SubCategory title={title} key={title} traits={traits} mainState={mainState} />
            } else {
                return <div className={`editables ${mainState.mainState.characterCreation && 'not-fixed'} ${mainState.mainState.toggleDetails}`} key={title} >
                    <div onClick={this.closeDetails}>close</div>
                    <SubCategory title={title} traits={traits} mainState={mainState} />
                </div>
            }

         });
        return(
            <div className="main-traits">
                {renderTraitTitle}
            </div>
        );
    }
}

const mapStateToProps = ({ traits, mainState }) => {
    return {
        traits: traits,
        mainState: mainState
    }
}

export default connect(mapStateToProps, actions)(Category);