import React, { Component } from 'react';

import Traits from './traits';
import Editables from './editables';

class SubCategory extends Component {
    render() {
        const { traits, title} = this.props;

        const renderTraitTitle = Object.keys(traits.traits[title]).map( subtitle => {
            if(title !== "04editables") {
                return (
                    <Traits title={title} subtitle={subtitle} key={subtitle} traits={traits} />
                )
            } else {
                return (
                    <Editables title={title} name={subtitle} key={subtitle} traits={traits} />
                )
            }
         });

        return(
            <div className="traitsContanier">
                {title !== "04editables" && <h2>{title.replace(/\d/g,'')}</h2>}
                {renderTraitTitle}
            </div>
        );
    }
}

export default SubCategory;