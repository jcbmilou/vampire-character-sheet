import React, { Component }  from 'react';
import { connect } from 'react-redux';
import * as actions from '../../actions'

import IndividualTrait from './individualtrait';

class Traits extends Component {
    state = {
        inputValue: ""
    }

    setInputValue = (e) => {
        this.setState({inputValue: e.target.value});
    }

    addTrait = () => {
        const { updateTraits, subtitle, traits, title } = this.props;
        const newTrait = this.state.inputValue;

        if(newTrait.length > 0) {
            traits.traits[title][subtitle][newTrait] = [1];
            this.setState({inputValue: ""});
            updateTraits(traits);
        }
    }

    renderAddTraits = () => {
        const { title, subtitle, mainState } = this.props;
        
        if(mainState.mainState.characterCreation && title !== "01abilities" && subtitle !== "02willpower") {
            return(
                <React.Fragment>
                    <input placeholder={`Add New ${subtitle.replace(/\d/g,'')}`} onChange={e => {this.setInputValue(e)}} value={this.state.inputValue} />
                    <button onClick={this.addTrait}>Add</button>
                </React.Fragment>
            )
        } else {
            return
        }
    }

    render() {
        const { traits, title, subtitle } = this.props;

        const renderTraitTitle = Object.keys(traits.traits[title][subtitle]).map( name => {
            return <IndividualTrait traits={traits} title={title} subtitle={subtitle} name={name} key={name} />
         });

        return(
            <div className="traits-group">
                <h3>{subtitle.replace(/\d/g,'')}</h3>
                {renderTraitTitle}
                {this.renderAddTraits()}
            </div>
        );
    }
}

const mapStateToProps = ({ traits, mainState }) => {
    return {
        traits: traits,
        mainState: mainState,
    }
}

export default connect(mapStateToProps, actions)(Traits);