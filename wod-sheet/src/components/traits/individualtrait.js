import React, { Component }  from 'react';
import { connect } from 'react-redux';

import * as actions from '../../actions/index';

class IndividualTrait extends Component {

    value = this.props.traits.traits[this.props.title][this.props.subtitle][this.props.name]

    changeBullet = (action) => {
        const { traits, updateTraits, title, subtitle, name } = this.props;
        if(this.value[0] < 10 && action === 'add') {
            traits.traits[title][subtitle][name][0] = ++this.value[0]
        } else if(this.value[0] > 1 && action === 'remove') {
            traits.traits[title][subtitle][name][0] = --this.value[0]
        } else {
            return;
        }

        updateTraits(traits);
    }

    renderButtons = () => {
        return(
            <React.Fragment>
                {this.props.mainState.mainState.characterCreation &&
                        <div className="edit-buttons">
                            <button onClick={() => {this.changeBullet('add')}}>+</button>
                            <button onClick={() => {this.changeBullet('remove')}}>-</button>
                        </div>
                }
            </React.Fragment>
        )
    }   

    render() {
        const dots = [...Array(this.value[0])].map(
            (e, i) => <span className="dot" key={i}></span>
        );
        
        return (
            <React.Fragment>
                <div className="individual-trait">
                    <span className="trait-name">{this.props.name.replace(/\d/g,'')}: </span>
                    {dots}
                </div>
                {this.renderButtons()}
            </React.Fragment>
        )
    }
}

const mapStateToProps = ({ mainState }) => {
    return {
        mainState: mainState
    }
}

export default connect(mapStateToProps, actions)(IndividualTrait);