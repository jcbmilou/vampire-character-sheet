import React, { Component } from 'react';
import { connect } from 'react-redux';
import * as actions from '../../actions';
import { firestoreConnect} from 'react-redux-firebase';
import { compose } from 'redux';

import logo from '../../media/Vampirethemasquerade-logo.svg'

class Menu extends Component {
    state = {
        showCharacters: false,
        showNPCs: false
    }
    closeMenu = () => {
        this.props.mainState.mainState.menuOpen = false;

        this.props.updateMainState(this.props.mainState)
    }
    
    toogleShow = (e) => {
        if(e.target.parentElement.classList[0] === "characters-names") {
            this.setState((prevState) => ({
                showCharacters: !prevState.showCharacters,
                showNPCs: false
            }));
        } else if (e.target.parentElement.classList[0] === "npc-names") {
            this.setState((prevState) => ({
                showNPCs: !prevState.showNPCs,
                showCharacters: false
            }));
        }
    }

    startNewCharacter = (e) => {
        const {
            description,
            traits,
            mainState, 
            characters
        } = this.props;

        description.description = characters.ordered.defautCharacter[0].character.description.description;
        traits.traits = characters.ordered.defautCharacter[0].character.traits.traits;
        
        if(e.currentTarget.innerText.indexOf("New NPC") !== -1) {
            description.description.NPC = true;
        } else {
            description.description.NPC = false;
        }

        mainState.mainState.characterCreation = true;
        this.updateGLobalStates(traits, mainState, description);
        this.closeMenu();
    }

    openCharacter = (id) => {
        const {
            description,
            traits,
            mainState,
        } = this.props;

        const currentCharacter = this.props.characters.data.Characters[id].character

        description.description = currentCharacter.description.description;
        traits.traits = currentCharacter.traits.traits;
        mainState.mainState.characterCreation = false;
        this.updateGLobalStates(traits, mainState, description);
        this.closeMenu();
    }

    updateGLobalStates = (traits, mainState, description) => {
        const {
            updateDescription,
            updateTraits, 
            updateMainState,
        } = this.props;

        updateMainState(mainState);
        updateTraits(traits);
        updateDescription(description);
    }
    
    render() {
        const { mainState, characters } = this.props;
        const { showCharacters, showNPCs } = this.state;
        const charactersOrdered = characters.ordered.Characters;
        const defaultCharacter = characters.ordered.defautCharacter;
        return(
            <div className={`menu ${mainState.mainState.menuOpen ? 'open' : 'close '}`} >
                <div className="content">
                    <div className="logo">
                        <img src={logo} alt="logo"/>
                    </div>
                    <div className="new-character-menu">
                        <h3>Create New Character</h3>
                        {defaultCharacter ? 
                            <React.Fragment>
                                <div onClick={e => {this.startNewCharacter(e)}}>
                                    New Character
                                </div>
                                <div onClick={e => {this.startNewCharacter(e)}}>
                                    New NPC
                                </div>
                            </React.Fragment> :
                            <div className="loading">...loading</div>
                        }
                    </div>
                    <div className="load-characters"> 
                        <div className="characters-names">
                            <h3 onClick={e => {this.toogleShow(e)}}>Load Character</h3>
                            <div className={`names-list ${showCharacters ? 'show' : 'hide'}`}>
                                {charactersOrdered && charactersOrdered.map(character => {
                                    const description = character.character.description.description;
                                        return (
                                            !description.NPC && <div key={character.id} onClick={() => {this.openCharacter(character.id)}}>
                                                {description.main['01name']}
                                            </div>
                                        )
                                })}
                            </div>
                        </div>
                        <div className="npc-names">
                            <h3 onClick={e => {this.toogleShow(e)}}>Load NPC</h3>
                            <div className={`names-list ${showNPCs ? 'show' : 'hide'}`}>
                                {charactersOrdered && charactersOrdered.map(character => {
                                    const description = character.character.description.description;
                                        return (
                                            description.NPC && <div key={character.id} onClick={() => {this.openCharacter(character.id)}}>
                                                    {description.main['01name']}
                                                </div>
                                        )
                                })}
                            </div>
                        </div>
                    </div>
                </div>
                <div className="footer">Powered By Bogotá By Night <br/> since 1176</div>
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    const { traits, description, mainState, firestore} = state;

    return {
        traits: traits,
        description: description,
        mainState: mainState,
        characters: firestore
    }
  }

export default compose(
    connect(mapStateToProps, actions),
    firestoreConnect([
        { collection: 'Characters' },
        { collection: 'defautCharacter' }
    ])
)(Menu);