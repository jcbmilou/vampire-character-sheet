import React, { Component } from 'react';
import { connect } from 'react-redux';

import * as actions from '../actions';
import Category from './traits/category';
import MainDescription from './decription/mainDescription';
import Menu from './menu/menu';

import logo from '../media/Vampirethemasquerade-logo.svg'

class CharSheetApp extends Component {
  state = {
    saved: false
  }

  updateDataBase = () => {
    const { addNewCharacter, traits, description } = this.props;
    const character = {
      traits,
      description
    }
    addNewCharacter({character});
  }

  openMenu = () => {
    this.props.mainState.mainState.menuOpen = true;
    this.props.updateMainState(this.props.mainState)
  }

  openDetails = () => {
    this.props.mainState.mainState.toggleDetails = "";

    this.props.updateMainState(this.props.mainState)
  }

  render() {
    const { mainState } = this.props;
    return (
        <React.Fragment>
            {mainState.mainState.menuOpen ? <Menu /> :
            <div className="character-sheet">
                <div className="menu-icon" onClick={this.openMenu}>open</div>
                {!mainState.mainState.characterCreation && <div className="details-icon" onClick={this.openDetails}>Details</div>}
                <div className="logo">
                    <img src={logo} alt="logo"/>
                </div>
                <MainDescription />
                <Category />
                {mainState.mainState.characterCreation && 
                  <button onClick={this.updateDataBase}>Save</button>
                }
                {this.state.saved && <div>Saved</div>}
            </div>}
        </React.Fragment>
    );
  }
}

const mapStateToProps = ({ traits, description, mainState }) => {
  return {
      traits: traits,
      description: description,
      mainState: mainState
  }
}

export default connect(mapStateToProps, actions)(CharSheetApp);
