import React, { Component }  from 'react';
import { connect } from 'react-redux';
import * as actions from '../../actions';


class Category extends Component {
    updateInputState = (e, name) => {
        const { updateDescription, description } = this.props;
        const fieldName = name.name;

        description.description.main[fieldName] = e.target.value;
        updateDescription(description);
    }

    render() {
        const { description, mainState } = this.props;

        const mainDetails = description && Object.keys(description.description.main).map( (name) => {
            return (
                <div className="description-title" key={name}>
                    <span className="desc-name">{name.replace(/\d/g,'')}: </span> 
                    <span>
                        {mainState.mainState.characterCreation ? 
                            <input placeholder={ description.description.main[name] } onChange={(e) => this.updateInputState(e, {name})} ></input> : 
                            description.description.main[name] 
                        }
                    </span>
                </div>
            )
         });
        return(
            <div className="main-description">
                {mainDetails}
            </div>
        );
    }
}

const mapStateToProps = ({ description, mainState }) => {
    return {
        description: description,
        mainState: mainState
    }
}

export default connect(mapStateToProps, actions)(Category);