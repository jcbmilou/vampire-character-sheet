export const updateMainState = (mainState)=> {
    return {
        type: 'UPDATE_MAIN_STATE',
        mainState
    }
}

export const updateTraits = (traits) => {
    return {
        type: 'UPDATE_TRAITS',
        traits
    }
}

export const updateDescription = (description) => {
    return {
        type: 'UPDATE_DESCRIPTION',
        description
    }
}

export const updateCharacter = (traits, description, firestore) => {
    let currentChar = firestore.find(char => { 
        return char.character.description.description.main["01name"] === description.description.main["01name"]
    })

    return (dispatch, getState, { getFirebase, getFirestore}) => {
        const firestore = getFirestore();
        firestore.collection('Characters').doc(currentChar.id).update({
            'character.traits': {...traits}
        })
        .then(() => {
          dispatch({type: 'UPADTE_CHARACTER'});
        })
        .catch(err => {
          dispatch({type: 'ERROR', err});
        });
      }
}


export const addNewCharacter = (newCharacter) => {
  return (dispatch, getState, { getFirebase, getFirestore}) => {
    const firestore = getFirestore();
    firestore.collection('Characters').add({
        ...newCharacter
    })
    .then(() => {
      dispatch({type: 'CREATE_CHARACTER', newCharacter});
    })
    .catch(err => {
      dispatch({type: 'ERROR', err});
    });
  }
};

// export const completeToDo = (completeToDoId, uid) => async dispatch => {
//     character
//     .child(uid)
//     .child(completeToDoId)
//     .remove();
// };

// export const fetchToDos = uid => async dispatch => {
//     character.child(uid).on("value", snapshot => {
//     dispatch({
//       type: FETCH_TRAITS,
//       payload: snapshot.val()
//     });
//   });
// };



// export const fetchUser = () => dispatch => {
//   authRef.onAuthStateChanged(user => {
//     if (user) {
//       dispatch({
//         type: FETCH_USER,
//         payload: user
//       });
//     } else {
//       dispatch({
//         type: FETCH_USER,
//         payload: null
//       });
//     }
//   });
// };

// export const signIn = () => dispatch => {
//   authRef
//     .signInWithPopup(provider)
//     .then(result => {})
//     .catch(error => {
//       console.log(error);
//     });
// };

// export const signOut = () => dispatch => {
//   authRef
//     .signOut()
//     .then(() => {
//       // Sign-out successful.
//     })
//     .catch(error => {
//       console.log(error);
//     });
// };